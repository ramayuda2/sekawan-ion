import { IonCard, IonCardContent, IonCardHeader, IonContent, IonHeader, IonItem, IonLabel, IonList, IonLoading, IonPage, IonText, IonTitle, IonToolbar, useIonLoading, useIonRouter } from "@ionic/react";
import { useEffect, useState } from "react";

interface dataDetail{
    map(arg0: (index: any, key: any) => import("react/jsx-runtime").JSX.Element): import("react").ReactNode;
    title: string;
    id: string;
    userId: string;
    completed: boolean;
}

const Home: React.FC = () => {

    const [ data, setData ] = useState<dataDetail>([] as never);
    const [loading, setLoading] = useState(false);
    const router = useIonRouter()
    const url = 'https://jsonplaceholder.typicode.com/todos/';
    
    const Fetch = async () => {
        setLoading(true)
        const dataFetch = await fetch(url)
        setData(await dataFetch.json())
        setLoading(false)
    }

    const Navigate = (id: string) => {
        router.push(`/dashboard/detail/${id}`, 'forward', 'push')
    }

    useEffect(() => {
        Fetch()
    }, [])

    return (
        <IonPage>
            <IonHeader mode="ios" color="primary">
                <IonToolbar color="primary">
                    <IonTitle>
                        List Title
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    {data.map((index: any, key: any) =>
                        <>
                            <IonItem onClick={() => Navigate(index.id)}>
                                <IonLabel>{index.id}. {index.title}</IonLabel>
                            </IonItem>
                        </>
                    )} 
                </IonList>
            </IonContent>
            <IonLoading isOpen={loading} />
        </IonPage>
    )
}

export default Home;