import { IonRouterOutlet } from "@ionic/react";
import { RouteComponentProps, Route } from "react-router";
import Dashboard from "./home";
import Detail from "./detail";

const RouterHome: React.FC<RouteComponentProps> = ({match}) => {
    return (
        <IonRouterOutlet>
            <Route exact={true} path={`${match.url}`} component={Dashboard} />
            <Route path={`${match.url}/detail/:id`} component={Detail} />
        </IonRouterOutlet>
    );
}

export default RouterHome