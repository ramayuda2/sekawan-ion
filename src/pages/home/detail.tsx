import { IonBackButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonHeader, IonLabel, IonLoading, IonPage, IonText, IonTitle, IonToolbar } from "@ionic/react";
import axios from "axios";
import { useState, useEffect } from "react";
import { RouteComponentProps } from "react-router";
import './css/detail.css'

interface DetailPageProps extends RouteComponentProps<{
    id: string;
  }> {}

interface dataDetail{
    title: string;
    id: string;
    userId: string;
    completed: boolean;
}

const Detail: React.FC<DetailPageProps> = ({match, history}) => {
    const [ data, setData ] = useState<dataDetail>({} as dataDetail);
    const [ loading, setLoading ] = useState(false);

    const url = `https://jsonplaceholder.typicode.com/todos/${match.params.id}`

    const Fetch = async () => {
        setLoading(true)
        const dataFetch = await fetch(url)
        setData(await dataFetch.json())
        setLoading(false)
    }

    useEffect(() => {
        Fetch()
    }, [])

    return (
        <IonPage>
            <IonHeader mode="ios" color="primary">
                <IonToolbar mode="ios" color='primary'>
                    <IonButtons slot="start">
                        <IonBackButton></IonBackButton>
                    </IonButtons>
                    <IonTitle>
                        Detail
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            {data.title}
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <div className="mt-2">
                            <IonLabel>Id: {data.id}</IonLabel>
                        </div>
                        <div className="mt-2">
                            <IonLabel>userId: {data.userId}</IonLabel>
                        </div>
                        <div className="mt-2">
                            <IonLabel>completed: {data.completed ? 'true': 'false'}</IonLabel>
                        </div>
                    </IonCardContent>
                </IonCard>
            </IonContent>
            <IonLoading isOpen={loading}/>
        </IonPage>
    )
}

export default Detail;