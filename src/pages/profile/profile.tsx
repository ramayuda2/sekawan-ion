import { IonAvatar, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonRow, IonText, IonTitle, IonToolbar } from "@ionic/react"
import { logoLinkedin, logoGitlab, person } from "ionicons/icons";
import React from "react";
import './css/profile.css'

const Profile: React.FC = () => {

    const sosmed = [
        {
            'source': "linkdin",
            'icon': logoLinkedin,
            'url': "https://www.linkedin.com/in/rama-yuda-wardani-972893239/",
            'color': "primary"
        },
        {
            'source': "gitlab",
            'icon': logoGitlab,
            'url': "https://gitlab.com/ramayuda2",
            'color': "warning"
        },
        {
            'source': "Profile",
            'icon': person,
            'url': "https://ramayuda.vercel.app/",
            'color': "danger"
        },
    ]

    const freelance = [
        {
            'nama': 'Dangroup',
            'framawork': 'Django, Nextjs, Mysql, Docker',
            'project': 'Menambah Fitur dan maintenance'
        },
        {
            'nama': 'E-Audit Blitar',
            'framawork': 'Laravel, Mysql, Docker',
            'project': 'Menambah Fitur dan maintenance'
        },
        {
            'nama': 'WISH',
            'framawork': 'PHP Native, Mysql, Docker',
            'project': 'Remastered ERP'
        },
        {
            'nama': 'Koperasi Artha',
            'framawork': 'CI(PHP), Reactjs, Docker',
            'project': 'Menambah fitur dan Maintenance'
        },
    ]

    const skill = [
        {
            'nama': 'Django',
            'level': 'medium',
        },
        {
            'nama': 'Laravel',
            'level': 'medium',
        },
        {
            'nama': 'GIN (Golang)',
            'level': 'medium',
        },
        {
            'nama': 'React',
            'level': 'medium',
        },
        {
            'nama': 'NextJS',
            'level': 'medium',
        },
        {
            'nama': 'Mysql',
            'level': 'medium',
        },
        {
            'nama': 'Posgresql',
            'level': 'medium',
        },
        {
            'nama': 'MongoDB',
            'level': 'medium',
        },
        {
            'nama': 'HTML',
            'level': 'Skillful',
        },
        {
            'nama': 'Python',
            'level': 'Skillful',
        },
        {
            'nama': 'PHP',
            'level': 'Skillful',
        },
        {
            'nama': 'JavaScript',
            'level': 'Skillful',
        },
        {
            'nama': 'Golang',
            'level': 'Skillful',
        },
        {
            'nama': 'Css',
            'level': 'medium',
        },
    ]

    return (
        <React.Fragment>
            <IonHeader color="primary" mode="ios">
                <IonToolbar color='primary' mode="ios">
                    <IonTitle>
                        Profile
                    </IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <div className="mt-2 d-flex justify-center">
                    <IonAvatar>
                        <img src="/IMG_20230904_172331.jpg" alt="" />
                    </IonAvatar>
                </div>
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            Social Media
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <IonGrid>
                            <IonRow>
                                { sosmed.map((index, key) =>
                                    <IonCol className="center">
                                        <div>
                                            <IonIcon color={index.color} icon={index.icon} onClick={() => window.open(index.url)} size="large"></IonIcon>
                                        </div>
                                        <div>
                                            <IonLabel>{index.source}</IonLabel>
                                        </div>
                                    </IonCol>
                                ) }
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            Intership
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <IonGrid>
                            <IonRow>
                                <IonCol>
                                    <div>
                                        <IonLabel>Cv Adisatya IT Consultant (01-03-2022 ~ 01-07-2022)</IonLabel>
                                    </div>
                                    <IonCardSubtitle>
                                        Fullstack Developer
                                    </IonCardSubtitle>
                                    <IonText>Mengerjakan Issue yang ada pada suatu project dengan menggunakan framework Django, Postgresql, Docker</IonText>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            Freelance
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <IonGrid>
                            <IonRow>
                                <IonCol>
                                    <IonList>
                                        { freelance.map((index) => 
                                            <>
                                                <div>
                                                    <strong>{index.nama}</strong>
                                                </div>
                                                <div>
                                                    {index.framawork}
                                                </div>
                                                <div>
                                                    <IonText>
                                                        {index.project}
                                                    </IonText>
                                                </div>
                                            </>
                                        ) }
                                    </IonList>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
                <IonCard className="mb-60">
                    <IonCardHeader>
                        <IonCardTitle>
                            Skill
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <IonGrid>
                            <IonRow>
                                <IonCol>
                                    <IonList>
                                        { skill.map((index) => 
                                            <>
                                            <div className="d-flex justify-between">
                                                <div>
                                                    <strong>{index.nama}</strong>
                                                </div>
                                                <div>
                                                    {index.level}
                                                </div>
                                            </div>
                                            </>
                                        ) }
                                    </IonList>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </React.Fragment>
    )
}

export default Profile;