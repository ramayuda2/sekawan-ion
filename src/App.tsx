import { Link, Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { home, person } from 'ionicons/icons';
import RouterHome from './pages/home/router';
import Profile from './pages/profile/profile';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route path='/dashboard' render={ props => <RouterHome {...props} />} />
          <Route path='/user'>
            <Profile />
          </Route>
          <Redirect exact path='/' to='/dashboard' />
        </IonRouterOutlet>
        
        <IonTabBar slot="bottom" mode='ios'>
          <IonTabButton mode='ios' tab="dashboard" href='/dashboard'>
            <IonIcon aria-hidden="true" icon={home} />
            <IonLabel>Beranda</IonLabel>
          </IonTabButton>
          <IonTabButton mode='ios' tab="user" href="/user">
            <IonIcon aria-hidden="true" icon={person} />
            <IonLabel>User</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
